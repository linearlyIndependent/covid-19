# Covid-19

A demo Java/Android application created for the Android App Development Credit Course.

The application fetches data from the [COVID2019-API](https://github.com/nat236919/Covid2019API) API and is based on the Sunshine App from Udacity's [Developing Android Apps](https://classroom.udacity.com/courses/ud851) course. The pie chart view was taken from [Android Tutorial for Beginners: Create a Pie Chart With XML](https://medium.com/@evanca/android-tutorial-for-beginners-create-a-pie-chart-with-xml-36e67dabe67f) and modified by me so that it scales with screen size.
