/*
 * *
 *  * [Covid-19] SettingsMainFragment.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 4/7/20 12:17 AM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 4/6/20 11:04 PM.
 *
 */

package xyz.ploechl.covid_19;

import androidx.preference.CheckBoxPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

import android.content.SharedPreferences;
import android.os.Bundle;

public class SettingsMainFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);

        if (preference != null) {
            if (!(preference instanceof CheckBoxPreference)) {
                setPreferenceSummary(preference, sharedPreferences.getString(key, ""));
            }
        }
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preference_main);
        SharedPreferences preferences = getPreferenceScreen().getSharedPreferences();
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        int count = preferenceScreen.getPreferenceCount();

        for (int i = 0; i < count; i++) {
            Preference pref = preferenceScreen.getPreference(i);

            if (!(pref instanceof CheckBoxPreference)) {
                String value = preferences.getString(pref.getKey(), "");
                setPreferenceSummary(pref, value);

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    private  void setPreferenceSummary(Preference preference, Object value) {
        String label = value.toString();
        //String key = preference.getKey();

        if (preference instanceof ListPreference) {
            // Get the label for the key from the array and set it to the label from the other array.
            ListPreference listPreference = (ListPreference) preference;

            int preferenceIndex = listPreference.findIndexOfValue(label);

            if (preferenceIndex >= 0) {
                preference.setSummary(listPreference.getEntries()[preferenceIndex]);
            }
        } else {
            preference.setSummary(label);
        }
    }
}
