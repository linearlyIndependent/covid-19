/*
 * *
 *  * [Covid-19] CovidAdapter.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 4/3/20 2:34 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 4/3/20 2:34 PM.
 *
 */

package xyz.ploechl.covid_19.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import xyz.ploechl.covid_19.R;
import xyz.ploechl.covid_19.model.CovidCountryModel;
import xyz.ploechl.covid_19.model.MetadataContainer;

/**
 * Exposes an array of {@link CovidCountryModel}s wrapped in {@link MetadataContainer}s to a {@link RecyclerView}
 */
public class CovidAdapter extends RecyclerView.Adapter<CovidAdapter.CovidAdapterViewHolder> {
    /**
     * Contains the array of {@link CovidCountryModel}s wrapped in {@link MetadataContainer}s.
     */
    private MetadataContainer<CovidCountryModel[]> mCovidCountriesData;

    final private CovidAdapterOnClickHandler mClickHandler;

    public CovidAdapter(CovidAdapterOnClickHandler clickHandler) {
        this.mClickHandler = clickHandler;
    }

    /**
     * Gets called when the ViewHolder is created and creates the necessary amount of ViewHolders for the screen size.
     *
     * @param parentViewGroup   The parent ViewGroup.
     * @param viewType          Used to differentiate between item types if the ViewGroup has more than one type. Not used here.
     * @return                  A new {@link CovidAdapterViewHolder} that holds the Views.
     */
    @NonNull
    @Override
    public CovidAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        Context context = parentViewGroup.getContext();
        int listItemLayoutId = R.layout.covid_list_item;
        boolean attachToParentImmediately = false;

        return new CovidAdapterViewHolder(LayoutInflater.from(context).inflate(listItemLayoutId, parentViewGroup, attachToParentImmediately));
    }

    /**
     * Called by the {@link RecyclerView} to display the data at the given position.
     *
     * @param holder    The {@link androidx.recyclerview.widget.RecyclerView.ViewHolder} which gets updated with the data for the given position.
     * @param position  The position of the item in the dataset.
     */
    @Override
    public void onBindViewHolder(@NonNull CovidAdapterViewHolder holder, int position) {
        CovidCountryModel model = mCovidCountriesData.getModel()[position];

        holder.mCountryTextView.setText(model.getName());
        holder.mConfirmedCountTextView.setText(Integer.toString(model.getConfirmed()));
        holder.mDeathsCountTextView.setText(Integer.toString(model.getDeaths()));
        holder.mRecoveredCountTextView.setText(Integer.toString(model.getRecovered()));
        holder.mActiveCountTextView.setText(Integer.toString(model.getActive()));
    }

    /**
     * Returns the number of items in the dataset.
     *
     * @return Rhe number of items in the dataset.
     */
    @Override
    public int getItemCount() {
        if (this.mCovidCountriesData == null) {
            return 0;
        } else {
            return this.mCovidCountriesData.getModel().length;
        }
    }

    public MetadataContainer<CovidCountryModel[]> getmCovidCountriesData() {
        return mCovidCountriesData;
    }

    /**
     * Sets the CovidCountries data on the {@link CovidAdapter} without creating a new adapter.
     *
     * @param mCovidCountriesData   The new data.
     */
    public void setCovidCountriesData(MetadataContainer<CovidCountryModel[]> mCovidCountriesData) {
        this.mCovidCountriesData = mCovidCountriesData;
        notifyDataSetChanged();
    }

    /**
     * Interface for the onClick messages.
     */
    public interface CovidAdapterOnClickHandler {
        void onClick(CovidCountryModel clickedCountry);
    }

    /**
     * Manages the child views of a list item.
     */
    public class CovidAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView mCountryTextView;
        public final TextView mConfirmedCountTextView;
        public final TextView mDeathsCountTextView;
        public final TextView mRecoveredCountTextView;
        public final TextView mActiveCountTextView;

        public CovidAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            this.mCountryTextView = (TextView) itemView.findViewById(R.id.tv_country);
            this.mConfirmedCountTextView = (TextView) itemView.findViewById(R.id.tv_confirmed_count);
            this.mDeathsCountTextView = (TextView) itemView.findViewById(R.id.tv_deaths_count);
            this.mRecoveredCountTextView = (TextView) itemView.findViewById(R.id.tv_recovered_count);
            this.mActiveCountTextView = (TextView) itemView.findViewById(R.id.tv_active_count);

            itemView.setOnClickListener(this);
        }

        /**
         * Is called when the child view when it is clicked.
         *
         * @param v The child view that was clicked.
         */
        @Override
        public void onClick(View v) {
            mClickHandler.onClick(mCovidCountriesData.getModel()[getAdapterPosition()]);
        }
    }
}
