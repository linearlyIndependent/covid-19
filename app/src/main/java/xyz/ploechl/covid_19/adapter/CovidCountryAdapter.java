/*
 * *
 *  * [Covid-19] CovidCountryAdapter.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 4/5/20 10:39 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 4/5/20 10:39 PM.
 *
 */

package xyz.ploechl.covid_19.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.text.MessageFormat;

import xyz.ploechl.covid_19.R;
import xyz.ploechl.covid_19.model.CovidCountryModel;
import xyz.ploechl.covid_19.model.CovidModel;
import xyz.ploechl.covid_19.model.MetadataContainer;


/**
 * Exposes a {@link xyz.ploechl.covid_19.model.CovidCountryModel} and compares it to a {@link xyz.ploechl.covid_19.model.CovidModel} with the total world numbers to a {@link androidx.recyclerview.widget.RecyclerView}.
 */
public class CovidCountryAdapter extends RecyclerView.Adapter<CovidCountryAdapter.CovidAdapterViewHolder> {
    private static final String GLOBAL_PERCENTAGE_PATTERN = "{0}%";
    private static final String NAME_PATTERN = "{0}\n({1})";
    private static final DecimalFormat GLOBAL_PERCENTAGE_DECIMAL_PATTERN = new DecimalFormat("#.###");
    private Context mContext;
    private MetadataContainer<CovidCountryModel> mCovidCountryData;
    private MetadataContainer<CovidModel> mCovidWorldData;

    public CovidCountryAdapter(Context context) {
        this.mContext = context;
    }

    public MetadataContainer<CovidCountryModel> getmCovidCountryData() {
        return mCovidCountryData;
    }

    public MetadataContainer<CovidModel> getmCovidWorldData() {
        return mCovidWorldData;
    }

    @NonNull
    @Override
    public CovidAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int listItemLayoutId = R.layout.pie_chart;
        boolean attachToParentImmediately = false;

        return new CovidAdapterViewHolder(LayoutInflater.from(context).inflate(listItemLayoutId, parent, attachToParentImmediately));
    }

    @Override
    public void onBindViewHolder(@NonNull CovidAdapterViewHolder holder, int position) {
        int country = 0;
        int world = 0;
        int progressBarForeground = 0;
        String title = "";
        //Drawable progressBarCircle = ContextCompat.getDrawable(this.mContext, R.drawable.stats_progress);
        //MessageFormat.format(

        switch (position) {
            case 0:
                country = this.mCovidCountryData.getModel().getConfirmed();
                world = this.mCovidWorldData.getModel().getConfirmed();

                title = this.mContext.getString(R.string.confirmed);
                progressBarForeground = ContextCompat.getColor(this.mContext, R.color.confirmedForeground);
                break;
            case 1:
                country = this.mCovidCountryData.getModel().getDeaths();
                world = this.mCovidWorldData.getModel().getDeaths();

                title = this.mContext.getString(R.string.deaths);
                progressBarForeground = ContextCompat.getColor(this.mContext, R.color.deathsForeground);
                /*Drawable a = ContextCompat.getDrawable(this.mContext, R.drawable.stats_progress);
                a.setColorFilter(new LightingColorFilter(0xFFFF0000, ContextCompat.getColor(this.mContext, R.color.deathsForeground)));
                holder.mStatsProgressBar.setProgressDrawable(a);*/
                break;
            case 2:
                country = this.mCovidCountryData.getModel().getRecovered();
                world = this.mCovidWorldData.getModel().getRecovered();

                title = this.mContext.getString(R.string.recovered);
                progressBarForeground = ContextCompat.getColor(this.mContext, R.color.recoveredForeground);
                break;
            case 3:
                country = this.mCovidCountryData.getModel().getActive();
                world = this.mCovidWorldData.getModel().getActive();

                title = this.mContext.getString(R.string.active);
                progressBarForeground = ContextCompat.getColor(this.mContext, R.color.activeForeground);
                break;
            default:

        }

        holder.mChartTitleTextView.setText(MessageFormat.format(NAME_PATTERN, title, this.mContext.getString(R.string.global_share)));

        holder.mNumberOfCasesTextView.setText(MessageFormat.format("{0}/{1}", country, world));
        holder.mStatsProgressBar.getProgressDrawable().setColorFilter(new LightingColorFilter(0xFFFF0000, progressBarForeground));

        if (world == 0) {
            holder.mStatsProgressBar.setProgress(0,true);
        } else {
            double progressDouble = ((double)country / (double)world)*100.0;
            int progress = (int)Math.floor(progressDouble);

            holder.mPercentageOfCasesTextView.setText(MessageFormat.format(GLOBAL_PERCENTAGE_PATTERN, GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(progressDouble)));

            if (progress == 0 && country != 0) {
                progress = 1;
            }

            holder.mStatsProgressBar.setProgress(progress);
        }
    }

    @Override
    public int getItemCount() {
        // confirmed, dead, active, recovered
        return 4;
    }

    public void setCovidData(MetadataContainer<CovidCountryModel> countryModel, MetadataContainer<CovidModel> worldModel) {
        this.mCovidCountryData = countryModel;
        this.mCovidWorldData = worldModel;
        notifyDataSetChanged();
    }


    public class CovidAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mChartTitleTextView;
        private TextView mNumberOfCasesTextView;
        private TextView mPercentageOfCasesTextView;
        private ProgressBar mStatsProgressBar;

        public CovidAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            this.mChartTitleTextView = (TextView) itemView.findViewById(R.id.chart_title);
            this.mNumberOfCasesTextView = (TextView) itemView.findViewById(R.id.number_of_cases);
            this.mPercentageOfCasesTextView = (TextView) itemView.findViewById(R.id.percentage_of_cases);
            this.mStatsProgressBar = (ProgressBar) itemView.findViewById(R.id.stats_progressbar);
        }
    }
}
