/*
 * *
 *  * [Covid-19] JsonParser.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 3/29/20 6:48 PM.
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/29/20 6:48 PM.
 *
 */

package xyz.ploechl.covid_19.parsers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.NoSuchElementException;

import xyz.ploechl.covid_19.model.CovidCountryModel;
import xyz.ploechl.covid_19.model.CovidModel;
import xyz.ploechl.covid_19.model.MetadataContainer;

/**
 * A static class that handles the JSON parsing for the application.
 */
public final class JsonParser {
    private static final String CLASS_NAME = JsonParser.class.getSimpleName();
    //private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy");

    /**
     * This level 0 key retrieves the data of the JSON.
     */
    private static final String L0_DATA = "data";

    /**
     * This level 0 key retrieves the timestamp when
     * the data retrived in the JSON was last modified.
     */
    private static final String L0_DT = "dt";

    /**
     * This level 0 key retrieves the TS of the JSON.
     */
    private static final String L0_TS = "ts";

    /**
     * This and other level 1 keys retrieve data from the data array.
     * <p>
     * L1_LOCATION holds a {@link String}, the other L1 keys hold an {@link int}.
     */
    private static final String L1_LOCATION = "location";
    private static final String L1_CONFIRMED = "confirmed";
    private static final String L1_DEATHS = "deaths";
    private static final String L1_RECOVERED = "recovered";
    private static final String L1_ACTIVE = "active";

    /**
     * Parses a JSON string int JSON objects and retrieves the values into the objects.
     *
     * @param jsonString    The JSON string to be parsed.
     * @return              The object containing the parsed data.
     * @throws JSONException            If the JSON is malformed or does not contain the expected data at the expected format and place.
     * @throws NoSuchElementException   If the JSON is malformed or does not contain the expected data at the expected format and place.
     * @throws DateTimeParseException   If a datetime could not be parsed.
     */
    public static MetadataContainer<CovidCountryModel[]> getCountriesCurrentFromJson(String jsonString) throws JSONException, NoSuchElementException, DateTimeParseException {
        JSONObject parsedJson;

        parsedJson = new JSONObject(jsonString);

        Log.d(CLASS_NAME, Integer.toString(parsedJson.length()));

        if (parsedJson == null) {
            throw new JSONException("The parsed JSON object was null.");
        }

        MetadataContainer<CovidCountryModel[]> metadataContainer = new MetadataContainer<CovidCountryModel[]>();
        metadataContainer.setTs(parsedJson.getInt(L0_TS));
        metadataContainer.setDt(LocalDate.parse(parsedJson.getString(L0_DT), DATE_FORMATTER));

        JSONArray dataArray = parsedJson.getJSONArray(L0_DATA);
        CovidCountryModel[] models = new CovidCountryModel[dataArray.length()];

        for (int i = 0; i < dataArray.length(); i++) {
            JSONObject countryJSON = dataArray.getJSONObject(i);

            CovidCountryModel countryModel = new CovidCountryModel();
            countryModel.setName(countryJSON.getString(L1_LOCATION));
            countryModel.setActive(countryJSON.getInt(L1_ACTIVE));
            countryModel.setConfirmed(countryJSON.getInt(L1_CONFIRMED));
            countryModel.setDeaths(countryJSON.getInt(L1_DEATHS));
            countryModel.setRecovered(countryJSON.getInt(L1_RECOVERED));

            models[i] = countryModel;
        }

        metadataContainer.setModel(models);

        return metadataContainer;

        // I know this looks awful but if C# could do it with LINQ, I will also do it in Java.
        // I did not come here for compromises!
        //if (((JSON_KEYS.get(0).values()).stream()).allMatch(parsedJson::has)) {
        //
        //
        //  if (JSON_KEYS.get(1).values().stream().allMatch(e -> dataArray.)) {
        //
        //} else {
        //    throw new NoSuchElementException("getCountriesCurrentFromJson: The parsed JSON object is missing necessary L1 keys.");
        // }
        //
        //  } else {
        //      throw new NoSuchElementException("getCountriesCurrentFromJson: The parsed JSON object is missing necessary L0 keys.");
        //   }
    }

    /**
     * Parses a JSON string int JSON objects and retrieves the values into the objects.
     *
     * @param jsonString    The JSON string to be parsed.
     * @return              The object containing the parsed data.
     * @throws JSONException            If the JSON is malformed or does not contain the expected data at the expected format and place.
     * @throws NoSuchElementException   If the JSON is malformed or does not contain the expected data at the expected format and place.
     * @throws DateTimeParseException   If a datetime could not be parsed.
     */
    public static MetadataContainer<CovidModel> getWorldCurrentFromJson(String jsonString) throws JSONException, NoSuchElementException, DateTimeParseException {
        JSONObject parsedJson;

        parsedJson = new JSONObject(jsonString);

        Log.d(CLASS_NAME, Integer.toString(parsedJson.length()));

        if (parsedJson == null) {
            throw new JSONException("The parsed JSON object was null.");
        }

        MetadataContainer<CovidModel> metadataContainer = new MetadataContainer<CovidModel>();
        metadataContainer.setTs(parsedJson.getInt(L0_TS));
        metadataContainer.setDt(LocalDate.parse(parsedJson.getString(L0_DT), DATE_FORMATTER));

        JSONObject dataArray = parsedJson.getJSONObject(L0_DATA);
        CovidModel model = new CovidModel();
        model.setActive(dataArray.getInt(L1_ACTIVE));
        model.setConfirmed(dataArray.getInt(L1_CONFIRMED));
        model.setDeaths(dataArray.getInt(L1_DEATHS));
        model.setRecovered(dataArray.getInt(L1_RECOVERED));

        metadataContainer.setModel(model);

        return metadataContainer;
    }

    /**
     * Parses a JSON string int JSON objects and retrieves the values into the objects.
     *
     * @param jsonString    The JSON string to be parsed.
     * @return              The object containing the parsed data.
     * @throws JSONException            If the JSON is malformed or does not contain the expected data at the expected format and place.
     * @throws NoSuchElementException   If the JSON is malformed or does not contain the expected data at the expected format and place.
     * @throws DateTimeParseException   If a datetime could not be parsed.
     */
    public static MetadataContainer<CovidCountryModel> getSingleCountryCurrentFromJson(String jsonString) throws JSONException, NoSuchElementException, DateTimeParseException {
        JSONObject parsedJson;

        parsedJson = new JSONObject(jsonString);

        if (parsedJson == null) {
            throw new JSONException("The parsed JSON object was null.");
        }

        MetadataContainer<CovidCountryModel> metadataContainer = new MetadataContainer<CovidCountryModel>();
        metadataContainer.setTs(parsedJson.getInt(L0_TS));
        metadataContainer.setDt(LocalDate.parse(parsedJson.getString(L0_DT), DATE_FORMATTER));

        JSONObject dataArray = parsedJson.getJSONObject(L0_DATA);
        CovidCountryModel model = new CovidCountryModel();
        model.setName(dataArray.getString(L1_LOCATION));
        model.setActive(dataArray.getInt(L1_ACTIVE));
        model.setConfirmed(dataArray.getInt(L1_CONFIRMED));
        model.setDeaths(dataArray.getInt(L1_DEATHS));
        model.setRecovered(dataArray.getInt(L1_RECOVERED));

        metadataContainer.setModel(model);

        return metadataContainer;
    }
}
