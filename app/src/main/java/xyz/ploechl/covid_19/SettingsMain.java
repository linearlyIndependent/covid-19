/*
 * *
 *  * [Covid-19] SettingsMain.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 4/6/20 5:08 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 4/6/20 5:08 PM.
 *
 */

package xyz.ploechl.covid_19;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsMain extends AppCompatActivity {
    private static final String CLASS_NAME = CountryDetail.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        /*
        //TODO: maybe delete this.
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        */

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                Log.d(CLASS_NAME, "unexpected option selected: "+item.getItemId());
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    //TODO: maybe delete this.
    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }
    }
     */
}