/*
 * *
 *  * [Covid-19] CovidCountryModel.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 3/29/20 9:39 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 3/29/20 9:39 PM.
 *
 */

package xyz.ploechl.covid_19.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class CovidCountryModel extends CovidModel implements Parcelable {
    private String name;

    public CovidCountryModel() {

    }

    protected CovidCountryModel(Parcel in) {
        super(in);
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static CovidCountryModel fixActiveCount(CovidCountryModel countryModel) {
        int rightValue = countryModel.getConfirmed() - countryModel.getRecovered() - countryModel.getDeaths();

        if (countryModel.getActive() != rightValue) {
            countryModel.setActive(rightValue);
        }

        return countryModel;
    }

    public static final Creator<CovidCountryModel> CREATOR = new Creator<CovidCountryModel>() {
        @Override
        public CovidCountryModel createFromParcel(Parcel in) {
            return new CovidCountryModel(in);
        }

        @Override
        public CovidCountryModel[] newArray(int size) {
            return new CovidCountryModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return new StringBuilder()
                .append("country: ").append(this.name).append(", ")
                .append(super.toString()).toString();
    }
}