/*
 * *
 *  * [Covid-19] CovidModel.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 3/29/20 9:34 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 3/29/20 9:34 PM.
 *
 */

package xyz.ploechl.covid_19.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class CovidModel implements Parcelable {
    private int confirmed;
    private int deaths;
    private int recovered;
    private int active;

    public CovidModel() {

    }

    protected CovidModel(Parcel in) {
        confirmed = in.readInt();
        deaths = in.readInt();
        recovered = in.readInt();
        active = in.readInt();
    }

    public static final Creator<CovidModel> CREATOR = new Creator<CovidModel>() {
        @Override
        public CovidModel createFromParcel(Parcel in) {
            return new CovidModel(in);
        }

        @Override
        public CovidModel[] newArray(int size) {
            return new CovidModel[size];
        }
    };

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getRecovered() {
        return recovered;
    }

    public void setRecovered(int recovered) {
        this.recovered = recovered;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @NonNull
    @Override
    public String toString() {
        return new StringBuilder()
                .append("confirmed: ").append(this.confirmed).append(", ")
                .append("deaths: ").append(this.deaths).append(", ")
                .append("recovered: ").append(this.recovered).append(", ")
                .append("active: ").append(this.active).toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(confirmed);
        dest.writeInt(deaths);
        dest.writeInt(recovered);
        dest.writeInt(active);
    }
}