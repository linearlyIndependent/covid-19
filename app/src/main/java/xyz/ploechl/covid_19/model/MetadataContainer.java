/*
 * *
 *  * [Covid-19] MetadataContainer.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 3/29/20 9:16 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 3/29/20 9:16 PM.
 *
 */

package xyz.ploechl.covid_19.model;

import android.media.audiofx.DynamicsProcessing;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.NonNull;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;

public class MetadataContainer<T> implements Parcelable {
    private static final String CLASS_NAME = MetadataContainer.class.getSimpleName();
    private static final Byte COVID_MODEL_ARRAY_ID = (byte) 0;
    private static final Byte COVID_COUNTRY_MODEL_ARRAY_ID = (byte) 1;
    private static final Byte COVID_MODEL_ID = (byte) 6;
    private static final Byte COVID_COUNTRY_MODEL_ID = (byte) 7;

    private T model;
    private LocalDate dt;
    private Integer ts;

    public MetadataContainer () {

    }

    //TODO: clean up this part later!
    protected MetadataContainer(Parcel in) {
        if (in.readByte() == 0) {
            this.ts = null;
        } else {
            this.ts = in.readInt();
        }

        if (in.readByte() == 0) {
            this.dt = null;
        } else {
            this.dt = LocalDate.ofEpochDay(in.readLong());
        }

        if (in.readByte() == 0) {
            this.model = null;
            Log.e(CLASS_NAME, "Model was missing, flag 0 in deserialization!");
        } else {
            byte typeByte = in.readByte();

            if (typeByte == COVID_MODEL_ARRAY_ID) {
                this.model = (T)in.createTypedArray(CovidModel.CREATOR);
            } else if (typeByte == COVID_COUNTRY_MODEL_ARRAY_ID) {
                this.model = (T)in.createTypedArray(CovidCountryModel.CREATOR);
            } else if (typeByte == COVID_MODEL_ID) {
                this.model = in.readParcelable(CovidModel.class.getClassLoader());
            } else if (typeByte == COVID_COUNTRY_MODEL_ID) {
                this.model = in.readParcelable(CovidCountryModel.class.getClassLoader());
            } else {
                Log.e(CLASS_NAME, "Invalid type flag in deserialization: " + typeByte);
            }

            /*
            switch (in.readByte()) {
                case 0:
                    this.model = (T)in.createTypedArray(CovidModel.CREATOR);
                    break;
                case 1:
                    this.model = (T)in.createTypedArray(CovidCountryModel.CREATOR);
                    break;
                default:
                    this.model = null;
                    Log.e(CLASS_NAME, "Invalid type flag!");
            }*/
        }
    }

    public static final Creator<MetadataContainer> CREATOR = new Creator<MetadataContainer>() {
        @Override
        public MetadataContainer createFromParcel(Parcel in) {
            return new MetadataContainer(in);
        }

        @Override
        public MetadataContainer[] newArray(int size) {
            return new MetadataContainer[size];
        }
    };

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public LocalDate getDt() {
        return dt;
    }

    public void setDt(LocalDate dt) {
        this.dt = dt;
    }

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("dataset: ")
                .append("dt: ").append(this.dt).append(", ")
                .append("ts: ").append(this.ts).append("\n\n");

        // I guess reflection always looks awful.
        if (this.model.getClass().isArray()) {
            Object[] arr = (Object[])this.model;

            for (Object item:arr) {
                builder.append(item.toString()).append("\n");
            }

            return builder.toString();

            //Type arrayType = this.model.getClass().getComponentType();
            //if (arrayType == CovidCountryModel.class) {
            //    return builder.append(Arrays.toString((CovidCountryModel[])this.model)).toString();
            //} else if (arrayType == CovidModel.class) {
            //    return builder.append(Arrays.toString((CovidModel[])this.model)).toString();
            //}
        }

        return builder.append(this.model.toString()).toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (this.ts == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(this.ts);
        }

        if (this.dt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(this.dt.toEpochDay());
        }

        // Forgive me God, for I have sinned.
        if (this.model.getClass().isArray() && this.getClass().getComponentType().isAssignableFrom(Parcelable.class)) {
                if (this.getClass().getComponentType() == CovidModel.class) {
                    dest.writeByte((byte) 1);
                    dest.writeByte(COVID_MODEL_ARRAY_ID);
                    dest.writeTypedArray((Parcelable[])this.model, 0);

                } else if (this.getClass().getComponentType() == CovidCountryModel.class) {
                    dest.writeByte((byte) 1);
                    dest.writeByte(COVID_COUNTRY_MODEL_ARRAY_ID);
                    dest.writeTypedArray((Parcelable[])this.model, 0);
                } else {
                    dest.writeByte((byte) 0);
                }

        } else if (this.model instanceof Parcelable) {
            Log.d(CLASS_NAME, this.model.getClass().toString());
            if (this.model.getClass() == CovidModel.class) {
                dest.writeByte((byte) 1);
                dest.writeByte(COVID_MODEL_ID);
                dest.writeParcelable((Parcelable)this.model, 0);

            } else if (this.model.getClass() == CovidCountryModel.class) {
                dest.writeByte((byte) 1);
                dest.writeByte(COVID_COUNTRY_MODEL_ID);
                dest.writeParcelable((Parcelable)this.model, 0);
            } else {
                dest.writeByte((byte) 0);
            }
        } else {
            Log.d(CLASS_NAME, this.model.getClass().toString());
            Log.d(CLASS_NAME, "NOT assignable");
            dest.writeByte((byte) 0);
        }

        /*
                if (this.model.getClass().isArray() && this.getClass().getComponentType().isAssignableFrom(Parcelable.class)) {
                if (this.getClass().getComponentType() == CovidModel.class) {
                    dest.writeByte((byte) 1);
                    dest.writeByte(COVID_MODEL_ID);
                    dest.writeTypedArray((Parcelable[])this.model, 0);

                } else if (this.getClass().getComponentType() == CovidCountryModel.class) {
                    dest.writeByte((byte) 1);
                    dest.writeByte(COVID_COUNTRY_MODEL_ID);
                    dest.writeTypedArray((Parcelable[])this.model, 0);
                }
        }
        * */
    }
}
