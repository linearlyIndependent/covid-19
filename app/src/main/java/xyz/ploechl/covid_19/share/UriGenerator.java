/*
 * *
 *  * [Covid-19] UriGenerator.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 4/4/20 12:10 AM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 4/4/20 12:10 AM.
 *
 */

package xyz.ploechl.covid_19.share;

import android.net.Uri;

import java.util.Optional;

/**
 * Static class to generate the {@link android.net.Uri}s used in different parts of the application, mainly in the UI.
 */
public final class UriGenerator {

    /**
     * Generates a Google Maps style geo Uri.
     *
     * @param latitude  The latitude.
     * @param longitude The longitude.
     * @param location  The location string, can probably be anything that can be resolved by Google Maps.
     * @return  The location Uri.
     *
     * @see <a href="https://stackoverflow.com/questions/21689848/build-uri-of-the-format-xy">android - Build Uri of the format X:Y - Stack Overflow</a>
     */
    public static Uri generateGeoUri(float latitude, float longitude, String location) {
        Uri.Builder builder = new Uri.Builder()
                .scheme("geo")
                .encodedOpaquePart("0,0");

        if (location != null) {
            builder.appendQueryParameter("q", location);
        }

        return builder.build();
    }

    /**
     * A wrapper that defaults the latitude and longitude to 0.
     *
     * @param location  The location string, can probably be anything that can be resolved by Google Maps.
     * @return  The location Uri.
     */
    public static Uri generateGeoUri(String location) {
        return generateGeoUri(0,0, location);
    }
}
