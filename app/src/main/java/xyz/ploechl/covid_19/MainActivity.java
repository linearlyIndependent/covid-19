/*
 * *
 *  * [Covid-19] MainActivity.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 3/28/20 8:05 PM.
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/28/20 8:05 PM.
 *
 */

package xyz.ploechl.covid_19;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import xyz.ploechl.covid_19.adapter.CovidAdapter;
import xyz.ploechl.covid_19.model.CovidCountryModel;
import xyz.ploechl.covid_19.model.CovidModel;
import xyz.ploechl.covid_19.model.MetadataContainer;
import xyz.ploechl.covid_19.network_io.NetworkIO;
import xyz.ploechl.covid_19.parsers.JsonParser;
import xyz.ploechl.covid_19.share.UriGenerator;

// TODO: BUG, when orientation changes after failed refresh view is empty
public class MainActivity extends AppCompatActivity
        implements CovidAdapter.CovidAdapterOnClickHandler, LoaderManager.LoaderCallbacks<MetadataContainer[]>, SwipeRefreshLayout.OnRefreshListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String CLASS_NAME = MainActivity.class.getSimpleName();
    private static final int COVID_LOADER_ID = 0;
    private static final String COVID_LOADER_BUNDLE_ID = "cache";
    private static final int COUNTRIES_DATA_ID = 0;
    private static final int WORLD_DATA_ID = 1;

    private static Comparator<CovidCountryModel> COUNTRIES_COMPARATOR;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private CovidAdapter mCovidAdapter;

    private Bundle covidCountryModelBundle;
    private MetadataContainer<CovidModel> worldCovidData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_corona_countries);
        this.mRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false));

        // Improves performance if used on layouts where the child layout size does not change based on the content.
        this.mRecyclerView.setHasFixedSize(true);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        this.mCovidAdapter = new CovidAdapter(this);

        this.mRecyclerView.setAdapter(this.mCovidAdapter);


        //TODO: uncomment settingsmanager registration later
        //PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        this.mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.main_swipe_refresh);
        this.mSwipeRefreshLayout.setOnRefreshListener(this);

        //this.mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        //    @Override
        //    public void onRefresh() {
        //        Log.i(CLASS_NAME, "onRefresh called from SwipeRefreshLayout");
        //
        //        LoaderManager.getInstance(this).restartLoader(COVID_LOADER_ID, null, this);
        //    }
        //});

        this.mSwipeRefreshLayout.setRefreshing(true);

        if (this.covidCountryModelBundle == null) {
            this.covidCountryModelBundle = new Bundle();
        }

        // Get the saved preference ordering from the previous session to set up the UI in the right ordering.
        String orderingKey = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.pref_orderings_key),"");
        COUNTRIES_COMPARATOR = this.getComparator(orderingKey);

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        // Initializes a new loader or reuses the existing one.
        LoaderManager.getInstance(this).initLoader(COVID_LOADER_ID, this.covidCountryModelBundle, MainActivity.this);
        Log.d(CLASS_NAME, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    @NonNull
    @Override
    public Loader<MetadataContainer[]> onCreateLoader(int id, @Nullable Bundle args) {
        return new AsyncTaskLoader<MetadataContainer[]>(this) {
            MetadataContainer[] cacheArray;
            Comparator<CovidCountryModel> comparator;

            @Override
            protected void onStartLoading() {
                if (this.cacheArray != null && this.cacheArray[COUNTRIES_DATA_ID] != null && this.cacheArray[WORLD_DATA_ID] != null) {
                    Log.d(CLASS_NAME, "cache: " + this.cacheArray[COUNTRIES_DATA_ID].toString());
                    Log.d(CLASS_NAME, "cache: " + this.cacheArray[WORLD_DATA_ID].toString());
                    // If the data was already fetched just return it from the "cache".
                    deliverResult(this.cacheArray);
                } else {
                    Log.d(CLASS_NAME, "check shit");
                    // If the bundle contains a "cache" from the previous loader, save it before attempting to fetch new data.
                    if (args != null && args.containsKey(COVID_LOADER_BUNDLE_ID)) {
                        Log.d(CLASS_NAME, "cast shit");
                        this.cacheArray = (MetadataContainer[]) args.getParcelableArray(COVID_LOADER_BUNDLE_ID);
                    }

                    Log.d(CLASS_NAME, "forceLoad");
                    forceLoad();
                }
            }

            @Nullable
            @Override
            public MetadataContainer[] loadInBackground() {
                URL covidCountriesUrl = null;
                URL covidWorldUrl = null;
                MetadataContainer[] resultArray = { null, null };

                try {
                    covidCountriesUrl = NetworkIO.buildUrl(NetworkIO.ApiPath.CURRENT, null);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.e(CLASS_NAME, e.getMessage(), e);
                }

                try {
                    covidWorldUrl = NetworkIO.buildUrl(NetworkIO.ApiPath.TOTAL, null);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.e(CLASS_NAME, e.getMessage(), e);
                }

                if (covidCountriesUrl == null && covidWorldUrl == null) {
                    return resultArray;
                }

                if (covidCountriesUrl != null) {
                    try {
                        String fetchedJsonString = NetworkIO.getDataFromHttpsUrl(covidCountriesUrl);
                        MetadataContainer<CovidCountryModel[]> countriesData = JsonParser.getCountriesCurrentFromJson(fetchedJsonString);

                        // Fixes when the numbers don't add up because the active count is 0.
                        if (countriesData.getModel() != null) {
                            for (CovidCountryModel countryData:countriesData.getModel()) {
                                if (countryData.getActive() == 0) {
                                    CovidCountryModel.fixActiveCount(countryData);
                                }
                            }
                        }

                        resultArray[COUNTRIES_DATA_ID] = countriesData;

                    } catch (IOException | JSONException | DateTimeParseException e) {
                        e.printStackTrace();
                        Log.e(CLASS_NAME, e.getMessage(), e);
                    }
                }

                if (covidWorldUrl != null) {
                    try {
                        String fetchedJsonString = NetworkIO.getDataFromHttpsUrl(covidWorldUrl);
                        MetadataContainer<CovidModel> worldData = JsonParser.getWorldCurrentFromJson(fetchedJsonString);
                        resultArray[WORLD_DATA_ID] = worldData;

                    } catch (IOException | JSONException | DateTimeParseException e) {
                        e.printStackTrace();
                        Log.e(CLASS_NAME, e.getMessage(), e);
                    }
                }

                return resultArray;
            }

            @Override
            public void deliverResult(@Nullable MetadataContainer[] data) {

                // Save data to "cache" if it is not null.
                if (data != null && data.length == 2
                        && data[COUNTRIES_DATA_ID] != null && data[COUNTRIES_DATA_ID].getModel() instanceof CovidCountryModel[]
                        && data[WORLD_DATA_ID] != null && data[WORLD_DATA_ID].getModel() instanceof CovidModel) {

                    CovidCountryModel[] countryArray = (CovidCountryModel[])data[COUNTRIES_DATA_ID].getModel();

                    if (this.comparator != COUNTRIES_COMPARATOR || (this.cacheArray != null && countryArray != this.cacheArray[COUNTRIES_DATA_ID].getModel())) {
                        this.comparator = COUNTRIES_COMPARATOR;
                        Arrays.sort(countryArray, this.comparator);

                        for (CovidCountryModel a:countryArray) {
                            Log.d(CLASS_NAME, a.toString());
                        }
                    }

                    this.cacheArray = data;
                }

                super.deliverResult(data);
            }
        };
    }

    @Override
    public void onLoadFinished(@NonNull Loader<MetadataContainer[]> loader, MetadataContainer[] data) {
        this.mSwipeRefreshLayout.setRefreshing(false);

        if (data != null && data.length == 2 && data[COUNTRIES_DATA_ID] != null && data[WORLD_DATA_ID] != null) {
            MetadataContainer<CovidCountryModel[]> countriesData = data[COUNTRIES_DATA_ID];

            this.worldCovidData = data[WORLD_DATA_ID];
            this.mCovidAdapter.setCovidCountriesData(countriesData);

            String status = new StringBuilder()
                    .append(getString(R.string.state_world)).append(": ").append(countriesData.getDt().toString()).append("\n")
                    .append(getString(R.string.state)).append(": ").append(countriesData.getDt().toString()).append("\n")
                    .append(getString(R.string.countries)).append(": ").append(countriesData.getModel().length).toString();
            Toast.makeText(this, status, Toast.LENGTH_LONG).show();

            // Cache this shit.
            this.covidCountryModelBundle.clear();
            this.covidCountryModelBundle.putParcelableArray(COVID_LOADER_BUNDLE_ID, data);

        } else {
            Log.e(CLASS_NAME, "onLoadFinished delivered a null dataset which was discarded.");
            Toast.makeText(this, R.string.error_refresh, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<MetadataContainer[]> loader) {
        //TODO: see if I need this.
    }

    @Override
    public void onClick(CovidCountryModel clickedCountry) {
        MetadataContainer<CovidCountryModel> country = new MetadataContainer<>();
        country.setModel(clickedCountry);
        country.setDt(this.mCovidAdapter.getmCovidCountriesData().getDt());
        country.setTs(this.mCovidAdapter.getmCovidCountriesData().getTs());


        Log.d(CLASS_NAME, "before bundleing");
        Log.d(CLASS_NAME, country.toString());
        Log.d(CLASS_NAME, this.worldCovidData.toString());


        Bundle detailBundle = new Bundle();
        detailBundle.putParcelable(CountryDetail.INTENT_BUNDLE_COUNTRY_DATA_ID, country);
        detailBundle.putParcelable(CountryDetail.INTENT_BUNDLE_WORLD_DATA_ID, this.worldCovidData);

        startActivity(new Intent(this, CountryDetail.class).putExtra(CountryDetail.INTENT_BUNDLE_ID, detailBundle));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.action_share_list).setIntent(this.createShareCovidListIntent());
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                this.mSwipeRefreshLayout.setRefreshing(true);
                LoaderManager.getInstance(this).restartLoader(COVID_LOADER_ID, this.covidCountryModelBundle, this);
                return true;
            case R.id.action_share_list:
                startActivity(this.createShareCovidListIntent());
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsMain.class));
                return true;
            default:
                Log.d(CLASS_NAME, "Not handled id in onOptionsSelected: " + item.getItemId());
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        Log.i(CLASS_NAME, "onRefresh called from SwipeRefreshLayout");

        LoaderManager.getInstance(this).restartLoader(COVID_LOADER_ID, this.covidCountryModelBundle, this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Comparator<CovidCountryModel> newComparator = this.getComparator(sharedPreferences.getString(key, ""));

        if (COUNTRIES_COMPARATOR != newComparator) {
            COUNTRIES_COMPARATOR = newComparator;

            if (this.mCovidAdapter.getmCovidCountriesData() != null && this.mCovidAdapter.getmCovidCountriesData().getModel() != null) {
                Arrays.sort(this.mCovidAdapter.getmCovidCountriesData().getModel(), COUNTRIES_COMPARATOR);
            }
        }
    }

    private Intent createShareCovidListIntent() {
        String covidData = getString(R.string.string_share_header) + "\n\n";

        if (this.mCovidAdapter != null && this.mCovidAdapter.getmCovidCountriesData() != null) {
            covidData += this.mCovidAdapter.getmCovidCountriesData().toString();
        } else {
            covidData += "<no data>";
        }

        Log.d(CLASS_NAME, covidData);

        return ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText(covidData).getIntent();
    }

    /**
     * Returns the suitable {@link Comparator} to reorder the list for the new ordering chosen in the settings.
     *
     * @param comparingType The ordering option key as defined in {@link R.array}.
     * @return  The suitable {@link Comparator} or ordering by number of confirmed cases in descending order if no match.
     */
    private Comparator<CovidCountryModel> getComparator(String comparingType) {
        if (comparingType.equals(getString(R.string.pref_orderings_name_asc))) {
            return Comparator.comparing(CovidCountryModel::getName);

        } else if (comparingType.equals(getString(R.string.pref_orderings_name_desc))) {
            return Comparator.comparing(CovidCountryModel::getName).reversed();

        } else if (comparingType.equals(getString(R.string.pref_orderings_confirmed_asc))) {
            return Comparator.comparingInt(CovidCountryModel::getConfirmed);

        } else if (comparingType.equals(getString(R.string.pref_orderings_confirmed_desc))) {
            return Comparator.comparingInt(CovidCountryModel::getConfirmed).reversed();

        } else if (comparingType.equals(getString(R.string.pref_orderings_deaths_asc))) {
            return Comparator.comparingInt(CovidCountryModel::getDeaths);

        } else if (comparingType.equals(getString(R.string.pref_orderings_deaths_desc))) {
            return Comparator.comparingInt(CovidCountryModel::getDeaths).reversed();

        } else if (comparingType.equals(getString(R.string.pref_orderings_recovered_asc))) {
            return Comparator.comparingInt(CovidCountryModel::getRecovered);

        } else if (comparingType.equals(getString(R.string.pref_orderings_recovered_desc))) {
            return Comparator.comparingInt(CovidCountryModel::getRecovered).reversed();

        } else if (comparingType.equals(getString(R.string.pref_orderings_active_asc))) {
            return Comparator.comparingInt(CovidCountryModel::getActive);

        } else if (comparingType.equals(getString(R.string.pref_orderings_active_desc))) {
            return Comparator.comparingInt(CovidCountryModel::getActive).reversed();

        } else {
            // Fallback
            Log.d(CLASS_NAME, getString(R.string.pref_orderings_recovered_desc));
            return Comparator.comparingInt(CovidCountryModel::getConfirmed).reversed();
        }

    }
}
