/*
 * *
 *  * [Covid-19] NetworkIO.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 3/28/20 8:04 PM.
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/28/20 8:04 PM.
 *
 */

package xyz.ploechl.covid_19.network_io;

import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * A static class that handles the network IO for the application.
 *
 * Retrieves data from the COVID2019-API (V2) API.
 *
 * @see  <a href="https://github.com/nat236919/Covid2019API">COVID2019-API (V2)</a>
 */
public final class NetworkIO {
    private static final String CLASS_NAME = NetworkIO.class.getSimpleName();
    private static final Uri BASE_URI = Uri.parse("https://covid2019-api.herokuapp.com/v2");

    /**
     * Builds the URL for getting the Covid-19 datasets.
     *
     * Base URL:
     * https://covid2019-api.herokuapp.com/v2
     *
     * @param path the {@link ApiPath} path segment of the resource
     * @param parameter a {@link String} query parameter if needed or null
     * @return     the resource URL as a {@link String}
     * @exception  MalformedURLException  if the URL was somehow invalid
     */
    public static URL buildUrl(NetworkIO.ApiPath path, String parameter) throws MalformedURLException {
        Uri.Builder uriBuilder = BASE_URI.buildUpon();
        uriBuilder.appendPath(path.getPath());

        if (parameter != null) {
            uriBuilder.appendPath(parameter);
        }

        URL url = new URL(uriBuilder.build().toString());

        Log.d("COVID-19", "url: " + url);
        return url;
    }

    /**
     * Gets an HTTPS response by reading only the first response line.
     *
     * Style based on my C# best-practice memories and some research how to do them in Java.
     * Inspiration:
     * https://www.infoq.com/news/2010/08/arm-blocks/
     * https://alvinalexander.com/blog/post/java/simple-https-example/
     *
     * @param url the {@link URL} of the requested resource
     * @return     the resource as a {@link String} if it was successfully
     *             fetched or null when the input
     *             stream was empty
     * @exception  IOException  if an I/O exception occurs
     */
    public static String getDataFromHttpsUrl(URL url) throws IOException {
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        String result;

        try (
                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            result = bufferedReader.readLine();
        } catch (Exception e) {
            Log.e(CLASS_NAME, e.getMessage(), e);
            throw(e);
        }

        if (result == null) {
            Log.e(CLASS_NAME, "getDataFromHttpsUrl returned null because it reached the end of the stream");
        }

        Log.d(CLASS_NAME, result);

        return result;
    }

    /**
     * Represents different resource paths that can be appended to the base URL in {@link xyz.ploechl.covid_19.network_io.NetworkIO}.
     *
     * @see  <a href="https://github.com/nat236919/Covid2019API">COVID2019-API (V2)</a>
     */
    public enum ApiPath {
        // Current all countries separate.
        CURRENT("current"),
        // Current global.
        TOTAL("total"),
        CONFIRMED("confirmed"),
        DEATHS("deaths"),
        RECOVERED("recovered"),
        ACTIVE("active"),
        COUNTRY("country"),
        TIMESERIES("timeseries");

        private String path;

        ApiPath(String path) {
            this.path = path;
        }

        public String getPath() {
            return this.path;
        }
    }
}

