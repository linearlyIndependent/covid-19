/*
 * *
 *  * [Covid-19] CountryDetail.java
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 4/5/20 4:33 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 4/5/20 4:33 PM.
 *
 */

package xyz.ploechl.covid_19;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.collection.SimpleArrayMap;
import androidx.core.app.ShareCompat;
import androidx.loader.app.LoaderManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.MessageFormat;

import xyz.ploechl.covid_19.adapter.CovidCountryAdapter;
import xyz.ploechl.covid_19.model.CovidCountryModel;
import xyz.ploechl.covid_19.model.CovidModel;
import xyz.ploechl.covid_19.model.MetadataContainer;
import xyz.ploechl.covid_19.share.UriGenerator;

public class CountryDetail extends AppCompatActivity {
    private static final String CLASS_NAME = CountryDetail.class.getSimpleName();
    private static final String GLOBAL_COUNT_PATTERN = "{0} ({1}): {2}% ({3} / {4})";
    private static final DecimalFormat GLOBAL_PERCENTAGE_DECIMAL_PATTERN = new DecimalFormat("#.###");
    public static final String INTENT_BUNDLE_ID = "bundle_data";
    public static final String INTENT_BUNDLE_WORLD_DATA_ID = "bundle_world_data";
    public static final String INTENT_BUNDLE_COUNTRY_DATA_ID = "bundle_country_data";

    private String countryName;
    private int confirmed;
    private int deaths;
    private int recovered;
    private int active;

    private TextView mTitleDetail;
    private TextView mConfirmedCount;
    private TextView mDeathsCount;
    private TextView mRecoveredCount;
    private TextView mActiveCount;

    private RecyclerView mRecyclerView;
    private CovidCountryAdapter mCovidCountryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_detail);

        this.mTitleDetail = (TextView) findViewById(R.id.title_detail);
        this.mConfirmedCount = (TextView) findViewById(R.id.tv_confirmed_count);
        this.mDeathsCount = (TextView) findViewById(R.id.tv_deaths_count);
        this.mRecoveredCount = (TextView) findViewById(R.id.tv_recovered_count);
        this.mActiveCount = (TextView) findViewById(R.id.tv_active_count);

        this.mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_corona_country_detail);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        this.mRecyclerView.setHasFixedSize(true);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));

        this.mCovidCountryAdapter = new CovidCountryAdapter(this);
        this.mRecyclerView.setAdapter(this.mCovidCountryAdapter);

        MetadataContainer<CovidCountryModel> country;
        MetadataContainer<CovidModel> world;

        Intent starterIntent = getIntent();

        if (starterIntent != null && starterIntent.hasExtra(INTENT_BUNDLE_ID)) {
            //TODO: Get here the data from the starter intent!
            Bundle bundle = starterIntent.getBundleExtra(INTENT_BUNDLE_ID);

            if (bundle.containsKey(INTENT_BUNDLE_WORLD_DATA_ID) && bundle.containsKey(INTENT_BUNDLE_COUNTRY_DATA_ID)) {
                country = bundle.getParcelable(INTENT_BUNDLE_COUNTRY_DATA_ID);
                world = bundle.getParcelable(INTENT_BUNDLE_WORLD_DATA_ID);

                this.countryName = country.getModel().getName();
                this.mTitleDetail.setText(this.countryName);
                this.confirmed = country.getModel().getConfirmed();
                this.mConfirmedCount.setText(Integer.toString(this.confirmed));
                this.deaths = country.getModel().getDeaths();
                this.mDeathsCount.setText(Integer.toString(this.deaths));
                this.recovered = country.getModel().getRecovered();
                this.mRecoveredCount.setText(Integer.toString(this.recovered));
                this.active = country.getModel().getActive();
                this.mActiveCount.setText(Integer.toString(this.active));

                this.mCovidCountryAdapter.setCovidData(country, world);
            }
        }

        //TODO: uncomment settingsmanager registration later
        //PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        Log.d(CLASS_NAME, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share_detail:
                    startActivity(this.createShareCovidCountryDetailIntent());
                break;
            case R.id.action_country_map:
                    Intent intent = this.createOpenLocationOnMapIntent(this.countryName);

                    if (intent != null) {
                        startActivity(intent);
                    } else {
                        Log.d(CLASS_NAME, "no app to open location in");
                        Toast.makeText(this, R.string.error_open_map, Toast.LENGTH_LONG).show();
                    }

                break;
            default:
                Log.d(CLASS_NAME, "unknown menu item: " + item.getItemId());
        }

        return super.onOptionsItemSelected(item);
    }

    private Intent createShareCovidCountryDetailIntent() {
        String confirmedPercentage;
        String deathsPercentage;
        String recoveredPercentage;
        String activePercentage;

        MetadataContainer<CovidCountryModel> country = this.mCovidCountryAdapter.getmCovidCountryData();
        MetadataContainer<CovidModel> world = this.mCovidCountryAdapter.getmCovidWorldData();

        if (world.getModel().getConfirmed() != 0) {
            confirmedPercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(((double)country.getModel().getConfirmed() / (double)world.getModel().getConfirmed())*100.0);
        } else {
            confirmedPercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(0);
        }

        if (world.getModel().getDeaths() != 0) {
            deathsPercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(((double)country.getModel().getDeaths() / (double)world.getModel().getDeaths())*100.0);
        } else {
            deathsPercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(0);
        }

        if (world.getModel().getRecovered() != 0) {
            recoveredPercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(((double)country.getModel().getRecovered() / (double)world.getModel().getRecovered())*100.0);
        } else {
            recoveredPercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(0);
        }

        if (world.getModel().getActive() != 0) {
            activePercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(((double)country.getModel().getActive() / (double)world.getModel().getActive())*100.0);
        } else {
            activePercentage = GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(0);
        }

        String shareString = new StringBuilder(getString(R.string.string_share_header)).append("\n\n")
                .append("dataset (world): dt:").append(world.getDt()).append(", ts: ").append(world.getTs()).append("\n\n")
                .append(country.toString()).append("\n\n")
                .append(MessageFormat.format(GLOBAL_COUNT_PATTERN, getString(R.string.confirmed), getString(R.string.global_share), confirmedPercentage, country.getModel().getConfirmed(), world.getModel().getConfirmed()))
                //.append(getString(R.string.world_count_percentage, getString(R.string.confirmed), getString(R.string.in_global), Integer.toString(confirmedPercentage), Integer.toString(country.getModel().getConfirmed()), Integer.toString(world.getModel().getConfirmed())))
                .append("\n")
                .append(MessageFormat.format(GLOBAL_COUNT_PATTERN, getString(R.string.deaths), getString(R.string.global_share), deathsPercentage, country.getModel().getDeaths(), world.getModel().getDeaths()))
                //.append(getString(R.string.world_count_percentage, getString(R.string.deaths), getString(R.string.in_global), Integer.toString(deathsPercentage), Integer.toString(country.getModel().getDeaths()), Integer.toString(world.getModel().getDeaths())))
                .append("\n")
                .append(MessageFormat.format(GLOBAL_COUNT_PATTERN, getString(R.string.recovered), getString(R.string.global_share), recoveredPercentage, country.getModel().getRecovered(), world.getModel().getRecovered()))
                //.append(getString(R.string.world_count_percentage, getString(R.string.recovered), getString(R.string.in_global), Integer.toString(recoveredPercentage), Integer.toString(country.getModel().getRecovered()), Integer.toString(world.getModel().getRecovered())))
                .append("\n")
                .append(MessageFormat.format(GLOBAL_COUNT_PATTERN, getString(R.string.active), getString(R.string.global_share), activePercentage, country.getModel().getActive(), world.getModel().getActive()))
                //.append(getString(R.string.world_count_percentage, getString(R.string.active), getString(R.string.in_global), Integer.toString(activePercentage), Integer.toString(country.getModel().getActive()), Integer.toString(world.getModel().getActive())))
                .toString();

        Log.d(CLASS_NAME, shareString);

        return ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText(shareString).getIntent();
    }

    private Intent createOpenLocationOnMapIntent(@NonNull String location) {
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(UriGenerator.generateGeoUri(location));

        if (intent.resolveActivity(getPackageManager()) == null) {
            return null;
        } else {
            return intent;
        }
    }
}
